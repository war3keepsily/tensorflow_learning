import tensorflow as tf

tf.enable_eager_execution()
x = tf.Variable(initial_value=tf.random_normal(shape=[3,7,7,3]))
w = tf.constant([[[[1,1,-1],[-1,0,1],[-1,-1,0]],
                  [[-1,0,-1],[0,0,-1],[1,-1,0]],
                  [[0,1,0],[1,0,1],[0,-1,1]]],
                 [[[1,2,-1],[-1,4,1],[-1,-1,0]],
                  [[-1,0,-1],[0,0,-1],[1,-1,0]],
                  [[0,1,0],[1,0,1],[0,-1,2]]]],dtype='float32')
# print(w.shape)
# 2*3*3*3

w = tf.transpose(w,perm=[3,1,2,0])
print(w.shape)
print('x =',x[:,:,:,0])
print('x1 = ',x[:,:,:,1])
print('x2 = ',x[:,:,:,2])

output = tf.nn.conv2d(x,w,strides=[1,2,2,1],padding="SAME",use_cudnn_on_gpu=False,)
print(output.shape)
print(output)
